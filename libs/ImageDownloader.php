<?php

/**
 * Class ImageDownloader
 */
class ImageDownloader
{

    protected $url = '';
    public $images = array();
    protected $imgFolder = '/images';

    /**
     * ImageDownloader constructor.
     * @param $url
     * @param null $imgFolder
     */
    public function __construct($url, $imgFolder = null)
    {
        $this->url = $url;

        if ($imgFolder) {
            $this->imgFolder = $imgFolder;
        }
    }

    /**
     * Gets the page html code and selects links from the tag img.
     * @return $this
     */
    public function getImagesByUrl()
    {
        $headers = array("Accept" => "text/html");

        $response = Unirest\Request::post($this->url, $headers);

        preg_match_all('/(img|src)=("|\')[^"\'>]+/i', $response->raw_body, $media);
        unset($response);
        $data = preg_replace('/(img|src)("|\'|="|=\')(.*)/i', "$3", $media[0]);

        foreach ($data as $url) {
            $info = pathinfo($url);
            if (isset($info['extension'])) {
                if (($info['extension'] == 'jpg') ||
                    ($info['extension'] == 'jpeg') ||
                    ($info['extension'] == 'gif') ||
                    ($info['extension'] == 'png')
                )
                    array_push($this->images, $url);
            }
        }
        return $this;
    }

    /**
     *  Saves images from an array of links
     */
    public function saveImages()
    {
        foreach ($this->images as $image) {
            $path = parse_url($image, PHP_URL_PATH);
            $host = parse_url($image, PHP_URL_HOST);
            if (empty($host)) {
                $host = parse_url($this->url, PHP_URL_HOST);
            }
            $scheme = parse_url($this->url, PHP_URL_SCHEME);

            $absoluteUrl = $scheme . '://' . $host . $path;
            $exp = explode("/", $image);
            $imageName = end($exp);


            if (!copy($absoluteUrl, $this->imgFolder . '/' . $imageName)) {
                Logger::getLogger('SaveImages')->log(['Error download', $absoluteUrl, $imageName]); //Writes to the log when a copy error occurs
            } else {
                echo $absoluteUrl . " --- Copied\n";
            }
        }
        return $this;
    }

    /**
     * Checks and fixes
     * @return string
     */
    function checkUrl()
    {
        $arUrl = parse_url($this->url);
        $parseUrl = null;

        // If no protocol was specified, or specified protocol is invalid for url
        if (!array_key_exists("scheme", $arUrl) || !in_array($arUrl["scheme"], array("http", "https"))) {
            // Set default Protocol - http
            $arUrl["scheme"] = "http";
        }


        // If the parse_url function could determine the host
        if (array_key_exists("host", $arUrl) && !empty($arUrl["host"])) {
            // Collect the final value of url
            $parseUrl = sprintf("%s://%s%s", $arUrl["scheme"], $arUrl["host"], $arUrl["path"]);
        }
        // If the host value is not defined
        // (usually this happens if no protocol is specified),
        // check $arUrl["path"] for the URL pattern matching.
        else if (preg_match("/^\w+\.[\w\.]+(\/.*)?$/", $arUrl["path"])) {
            // compile URL
            $parseUrl = sprintf("%s://%s", $arUrl["scheme"], $arUrl["path"]);
        }

        // If the url is valid and the query parameters string is passed
        if ($parseUrl && !empty($arUrl["query"])) {
            $parseUrl .= sprintf("?%s", $arUrl["query"]);
        }

        if (!empty($parseUrl)) {
            $this->url = $parseUrl;
        } else {
            Logger::getLogger('SaveImages')->log(['Invalid url format', 'Url:' . $this->url]);
            exit;
        }
        return $this;
    }

}