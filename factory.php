<?php

/**
 * Class LibsFactory
 */
class LibsFactory
{
    /**
     * @param $name : Class name
     * @param array $params :: Parameters for creating an object
     * @param bool $includeOnly :: If you do not want to create a class object
     * @throws Exception
     */
    static function factory($name, $params = array(), $includeOnly = false)
    {
        if (include 'libs/' . $name . '.php') {
            if ($includeOnly){
                return;
            }
            if (!empty($params)) {
                return new $name(...$params);
            } else {
                return new $name;
            }
        } else {
            throw new Exception('Wrong File');
        }
    }
}
