<?php
//Starting from the console: php /index.php http://url.com
//Starting from the browser: /index.php?url=http//url.com
require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once 'factory.php';
$programDirectory = dirname(__FILE__);

try {
    LibsFactory::factory('Logger',[], $includeOnly = true);
    Logger::$PATH = $programDirectory . '/logs'; //Folder with logs
    //Accept parameters from the console or from the address bar
    if (isset($argv) && !empty($argv)) {
        $url = $argv[1];
    } else {
        $url = $_GET['url'];
    }
    if (!isset($url) || empty($url)) {
        Logger::getLogger('SaveImages')->log("Missing parameter : url");
        echo "Missing parameter : url\n";
        echo "Starting from the console : php index.php 'URL'\nStarting from the browser : programUrl/index.php?url=url";
        return false;
    }

    $imgDownloader = LibsFactory::factory('ImageDownloader', [$url, $programDirectory . '/images']);
    $imgDownloader->checkUrl()->getImagesByUrl()->SaveImages();
} catch (Exception $e) {
    exit($e->getMessage());
}


